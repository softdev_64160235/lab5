/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab4;

/**
 *
 * @author Love_
 */
public class Lab4 {

    public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        while(true) {
            game.showBoard();
            game.showTurn();
            game.inputRowCol();
            if(game.isFinish()) {
                game.showBoard();
                game.showResult();
                game.showStat();
                game.newBoard();
            }
        }               
    }
}


